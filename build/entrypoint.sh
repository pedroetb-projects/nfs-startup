#!/bin/sh

echo "NFS startup"
echo "Populating \"${VOL_ADDR}${NFS_VOL_DEVICE}\" (mounted at: \"${NFS_ROOT_PATH}\")"

for directoryDefinition in ${NFS_STARTUP_DIRS}
do
    directoryName=$(echo "${directoryDefinition}" | cut -d ':' -f 1)
    directoryUid=$(echo "${directoryDefinition}:" | cut -d ':' -f 2)
    directoryGid=$(echo "${directoryDefinition}:" | cut -d ':' -f 3)

    directoryPath=${NFS_ROOT_PATH}/${directoryName}
    echo "Creating \"${directoryName}\" .."

    if [ -d ${directoryPath} ]
    then
        echo "  already exists!"
        directoryExists=1
    fi

    mkdir -p ${directoryPath}

    if [ -d ${directoryPath} ]
    then
        if [ -z ${directoryExists} ]
        then
            echo "  successfully created!"
        fi
    else
        echo "  creation failed!"
    fi

    if [ ! -z ${directoryUid} ]
    then
        echo "  set UID to \"${directoryUid}\""
        chown ${directoryUid} ${directoryPath}
    fi

    if [ ! -z ${directoryGid} ]
    then
        echo "  set GID to \"${directoryGid}\""
        chgrp ${directoryGid} ${directoryPath}
    fi
done

echo "Done!"
